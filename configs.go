// Licensed under the MIT license

package configs

import (
	"log"
	"time"

	arg "github.com/alexflint/go-arg"
)

var Args Configs

type Configs struct {
	Api
	Database
}

type Api struct {
	Name              string        `arg:"env:API_NAME" default:"policies"`
	Host              string        `arg:"env:API_HOST" default:"0.0.0.0"`
	Port              string        `arg:"env:API_PORT" default:"3333"`
	Version           string        `arg:"env:API_VERSION" default:"v0.1.0"`
	ReadTimeout       time.Duration `arg:"env:API_READ_TIMEOUT" default:"5s"`
	ReadHeaderTimeout time.Duration `arg:"env:API_READ_HEADER_TIMEOUT" default:"5s"`
	WriteTimeout      time.Duration `arg:"env:API_WRITE_TIMEOUT" default:"10s"`
	IdleTimeout       time.Duration `arg:"env:API_IDLE_TIMEOUT" default:"120s"`
	RequestLog        bool          `arg:"env:API_REQUEST_LOG" default:"true"`
}

type Database struct {
	Driver      string `arg:"env:DB_DRIVER" default:"postgres"`
	Host        string `arg:"env:DB_HOST" default:"localhost"`
	Port        string `arg:"env:DB_PORT" default:"5432"`
	Name        string `arg:"env:DB_NAME" default:"policies"`
	User        string `arg:"env:DB_USER" default:"user"`
	Pass        string `arg:"env:DB_PASS" default:"pass"`
	SslMode     string `arg:"env:DB_SSL_MODE" default:"disable"`
	MaxConnPool int    `arg:"env:DB_MAXX_CONN_POOL" default:"4"`
}

func (Configs) Version() string {
	return "Version: 1.0.0"
}

func New() *Configs {
	arg.MustParse(&Args)
	log.Printf("Config set to: %+v", Args)

	return &Args
}
