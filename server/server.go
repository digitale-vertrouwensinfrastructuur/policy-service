// Licensed under the MIT license

package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/go-chi/chi/v5"
	chiMiddleware "github.com/go-chi/chi/v5/middleware"

	"github.com/jmoiron/sqlx"
	configs "gitlab.com/digitale-vertrouwensinfrastructuur/policy-service"
	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/middleware"
)

type Server struct {
	cfg        *configs.Configs
	db         *sqlx.DB
	router     *chi.Mux
	httpServer *http.Server
}

func New() *Server {
	log.Println("Starting API")
	return &Server{}
}

func (s *Server) Init() {
	s.newConfigs()
	s.newRouter()
	s.initHealth()
	s.initPolicy()
}

func (s *Server) newConfigs() {
	s.cfg = configs.New()
}

func (s *Server) newRouter() {
	s.router = chi.NewRouter()
	s.router.Use(middleware.Cors)
	if s.cfg.Api.RequestLog {
		s.router.Use(chiMiddleware.Logger)
	}
	s.router.Use(chiMiddleware.Recoverer)
}

func (s *Server) Run() error {
	s.httpServer = &http.Server{
		Addr:           ":" + s.cfg.Api.Port,
		Handler:        s.router,
		ReadTimeout:    s.cfg.Api.ReadTimeout,
		WriteTimeout:   s.cfg.Api.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		printAllRegisteredRoutes(s.router)
		err := s.httpServer.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), s.cfg.Api.IdleTimeout)
	defer shutdown()

	return s.httpServer.Shutdown(ctx)
}

func (s *Server) GetConfig() *configs.Configs {
	return s.cfg
}

func (s *Server) GetDB() *sqlx.DB {
	return s.db
}

func printAllRegisteredRoutes(router *chi.Mux) {
	walkFunc := func(method string, path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("path: %s method: %s ", path, method)
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Print(err)
	}
}
