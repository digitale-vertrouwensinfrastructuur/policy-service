// Licensed under the MIT license

package server

import (
	policyHandler "gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy/handler/http"
	policyRepo "gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy/repository/inmemory"
)

func (s *Server) initPolicy() {
	s.initPolicies()
}

func (s *Server) initPolicies() {
	newPoliciesRepo := policyRepo.New(s.GetDB())
	policyHandler.RegisterHTTPEndPoints(s.router, newPoliciesRepo)
}
