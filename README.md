# Policy Service
The policy service is responsible for handling policies regarding data access.

## Requirements
- go 1.16+
## Building
### Build using go
```
go build ./...
```
### Build using Task
Install Task according to [Task installation](https://taskfile.dev/#/installation) and then run:
```
task build
```
More Task command can be shown using `task list`
## Running locally
### Run using go
```
go run ./cmd/policyservice.go
```
### Run using Skaffold
Install microk8s according to [Microk8s installation](https://microk8s.io/docs/install-alternatives)
Install Helm according to [Helm installation](https://helm.sh/docs/intro/install/)
Install Skaffold according to [Skaffold installation](https://skaffold.dev/docs/install/)
Copy the values from `microk8s.kubectl config view` to $HOME/.kube/config

```bash
docker login registry.gitlab.com # a gitlab access token can be used as password here

kubectl config use-context microk8s
kubectl create ns dvi-emergency-center
kubectl create ns dvi-digital-home-001
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-emergency-center
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-digital-home-001
```
This `regcred` secret will be used by the Helm deployment to fetch the Docker image. Make sure to create it in the correct namespace.

Alternatively, it is also possible to create the regcred using:
```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-emergency-center
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-digital-home-001
```

Then run:

```bash
skaffold dev --cleanup=false -p emergency-center
```

It may take some time for all resources to become healthy.

To test that the policy-api is up, make sure you can connect to it, for example using a port-forward

```bash
kubectl port-forward service/policy-service 8080:http --namespace=dvi-emergency-center
curl localhost:8080/health
```

To run the digital house policy service:
```bash
skaffold dev --cleanup=false -p digital-home

kubectl port-forward service/policy-service 8080:http --namespace=dvi-digital-home-001
curl localhost:8080/health
```
### Run using Docker-compose
```
docker-compose up
```
When running using docker-compose, there will be two policy-service containers running:
- digital house policy service on port 3080
- emergency center policy service on port 3085
## Testing
### Testing using Task
```
go test ./...
```
### Testing using Task
```
task test
```
### Testing REST endpoints manually
The `examples/policies.http` file can be used to test the REST endpoint

## License
See [LICENSE.md](LICENSE.md)
