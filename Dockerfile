FROM golang:1.16-alpine AS src

#RUN set -ex; \
#    apk update; \
#    apk --no-cache add ca-certificates git

WORKDIR /go/src/app/
COPY . ./

# Build Go Binary
RUN set -ex; \
    CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o ./policyservice ./cmd/policyservice/main.go;

FROM alpine

#RUN apt update
#RUN apt install net-tools htop

# Add new user 'appuser'. App should be run without root privileges as a security measure
RUN adduser --home "/home/appuser" --disabled-password appuser --gecos "appuser,-,-,-"
USER appuser
RUN mkdir -p /home/appuser/app
WORKDIR /home/appuser/app/

COPY --from=src /go/src/app/policyservice .

RUN pwd && ls -la && ls -la /home/appuser/app

EXPOSE 3333

# Run Go Binary
CMD /home/appuser/app/policyservice