// Licensed under the MIT license

package policy

import (
	"context"
)

type Repository interface {
	Create(ctx context.Context, p *Policy) (*Policy, error)
	All(ctx context.Context, f *Filter) ([]*Policy, error)
	Read(ctx context.Context, policyID int64) (*Policy, error)
	Update(ctx context.Context, p *Policy) error
	Delete(ctx context.Context, policyID int64) error
	Search(ctx context.Context, req *Filter) ([]*Policy, error)
}
