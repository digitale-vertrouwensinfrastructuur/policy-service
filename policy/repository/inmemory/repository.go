// Licensed under the MIT license

package inmemory

import (
	"context"
	"errors"
	"math/rand"

	"github.com/jmoiron/sqlx"

	policyModel "gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *repository {
	return &repository{db: db}
}

func (r *repository) Create(ctx context.Context, policy *policyModel.Policy) (*policyModel.Policy, error) {
	policy.PolicyID = int64(rand.Intn(100) + 10)
	policies = append(policies, policy)

	return policy, nil
}

func (r *repository) All(ctx context.Context, f *policyModel.Filter) ([]*policyModel.Policy, error) {
	return policies, nil
}

func (r *repository) Read(ctx context.Context, policyID int64) (*policyModel.Policy, error) {
	for _, p := range policies {
		if p.PolicyID == policyID {
			return p, nil
		}
	}

	return nil, errors.New("policy not found")
}

func (r *repository) Update(ctx context.Context, policy *policyModel.Policy) error {
	for i, p := range policies {
		if p.PolicyID == policy.PolicyID {
			policies[i] = policy
			return nil
		}
	}

	return errors.New("policy not found")
}

func (r *repository) Delete(ctx context.Context, policyID int64) error {
	for i, p := range policies {
		if p.PolicyID == policyID {
			policies = append((policies)[:i], (policies)[i+1:]...)
			return nil
		}
	}

	return errors.New("policy not found")
}

func (r *repository) Search(ctx context.Context, f *policyModel.Filter) ([]*policyModel.Policy, error) {
	// search and return list

	return policies, nil
}

var policies = []*policyModel.Policy{}
