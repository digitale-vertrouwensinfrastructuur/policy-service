// Licensed under the MIT license

package http

import (
	"context"
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"

	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy"
	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/utility/respond"
)

type Handler struct {
	repository policy.Repository
	validate   *validator.Validate
}

func NewHandler(repository policy.Repository) *Handler {
	return &Handler{
		repository: repository,
		validate:   validator.New(),
	}
}

// Create creates a new policy record
// @Summary Create a Policy
// @Description Get a policy with JSON payload
// @Accept json
// @Produce json
// @Param Policy body policy.Request true "Policy Request"
// @Success 201 {object} models.Policy
// @Router /api/v1/policies [post]
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var policyRequest policy.PolicyRequest
	err := json.NewDecoder(r.Body).Decode(&policyRequest)
	if err != nil {
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	errs := respond.Validate(h.validate, policyRequest)
	if errs != nil {
		respond.Error(w, http.StatusBadRequest, map[string][]string{"errors": errs})
		return
	}

	p, err := h.repository.Create(context.Background(), &policyRequest.Policy)
	if err != nil {
		respond.Error(w, http.StatusInternalServerError, err)
		return
	}

	pr, err := policy.NewPolicyResponse(p)
	if err != nil {
		respond.Error(w, http.StatusInternalServerError, err)
		return
	}
	respond.Render(w, http.StatusCreated, pr)
}

// Get a policy by its ID
// @Summary Get a Policy
// @Description Get a policy by its id.
// @Accept json
// @Produce json
// @Param policyID path int true "policy ID"
// @Success 200 {object} models.Policy
// @Router /api/v1/policies/{policyID} [get]
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	policyID, err := strconv.ParseInt(chi.URLParam(r, "policyID"), 10, 64)
	if err != nil {
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	p, err := h.repository.Read(context.Background(), policyID)
	if err != nil {
		if err == sql.ErrNoRows {
			respond.Error(w, http.StatusNoContent, nil)
		}
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	pr, err := policy.NewPolicyResponse(p)
	if err != nil {
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}
	respond.Render(w, http.StatusOK, pr)
}

// All will fetch the policies based on given params
// @Summary Show all policies
// @Description Get all policies. By default it gets first page with 10 items.
// @Accept json
// @Produce json
// @Param page query string false "page"
// @Param size query string false "size"
// @Param title query string false "term"
// @Param description query string false "term"
// @Success 200 {object} []Policy
// @Router /api/v1/policies [get]
func (h *Handler) All(w http.ResponseWriter, r *http.Request) {
	filters := policy.GetFilters(r.URL.Query())

	var policies []*policy.Policy
	ctx := context.Background()

	switch filters.Base.Search {
	case true:
		resp, err := h.repository.Search(ctx, filters)
		if err != nil {
			respond.Error(w, http.StatusInternalServerError, err)
			return
		}
		policies = resp
	default:
		resp, err := h.repository.All(ctx, filters)
		if err != nil {
			respond.Error(w, http.StatusInternalServerError, err)
			return
		}
		policies = resp
	}

	list, err := policy.NewPoliciesResponse(policies)
	if err != nil {
		respond.Render(w, http.StatusInternalServerError, map[string]string{"error": err.Error()})
		return
	} else if list == nil {
		respond.Error(w, http.StatusNoContent, nil)
		return
	}

	respond.Render(w, http.StatusOK, list)
}

// Update a policy
// @Summary Update a Policy
// @Description Update a policy by its model.
// @Accept json
// @Produce json
// @Param Policy body policy.Request true "Policy Request"
// @Success 200 "Ok"
// @Failure 500 "Internal Server error"
// @Router /api/v1/policies/{policyID} [put]
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	policyID, err := strconv.ParseInt(chi.URLParam(r, "policyID"), 10, 64)
	if err != nil {
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	var policyRequest policy.PolicyRequest
	err = json.NewDecoder(r.Body).Decode(&policyRequest)
	if err != nil {
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}
	policyRequest.PolicyID = policyID

	errs := respond.Validate(h.validate, policyRequest)
	if errs != nil {
		respond.Error(w, http.StatusBadRequest, map[string][]string{"errors": errs})
		return
	}

	err = h.repository.Update(context.Background(), &policyRequest.Policy)
	if err != nil {
		respond.Error(w, http.StatusInternalServerError, err)
		return
	}

	res, err := policy.NewPolicyResponse(&policyRequest.Policy)
	if err != nil {
		respond.Error(w, http.StatusInternalServerError, err)
		return
	}

	respond.Render(w, http.StatusOK, res)
}

// Delete a policy by its ID
// @Summary Delete a Policy
// @Description Delete a policy by its id.
// @Accept json
// @Produce json
// @Param id path int true "policy ID"
// @Success 200 "Ok"
// @Failure 500 "Internal Server error"
// @Router /api/v1/policies/{policyID} [delete]
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	policyID, err := strconv.ParseInt(chi.URLParam(r, "policyID"), 10, 64)
	if err != nil {
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	err = h.repository.Delete(context.Background(), policyID)
	if err != nil {
		respond.Error(w, http.StatusInternalServerError, nil)
		return
	}

	respond.Render(w, http.StatusOK, nil)
}
