// Licensed under the MIT license

package http

import (
	"github.com/go-chi/chi/v5"

	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/middleware"
	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy"
)

func RegisterHTTPEndPoints(router *chi.Mux, repo policy.Repository) {
	h := NewHandler(repo)

	router.Route("/api/v1/policies", func(router chi.Router) {
		router.Use(middleware.Json)
		router.Get("/", h.All)
		router.Get("/{policyID}", h.Get)
		router.Post("/", h.Create)
		router.Put("/{policyID}", h.Update)
		router.Delete("/{policyID}", h.Delete)
	})
}
