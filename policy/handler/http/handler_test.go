// Licensed under the MIT license

package http

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	chi "github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy"
	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/policy/mock"
	"gitlab.com/digitale-vertrouwensinfrastructuur/policy-service/utility/filter"
)

//go:generate mockgen -package mock -source ../../handler.go -destination=../../mock/mock_handler.go

func TestHandler_Create(t *testing.T) {
	testPolicyRequest := &policy.Policy{
		Actor: "Dummy user",
		Action: policy.Action{
			Scope:       "read:someinformation",
			Description: "Read some information",
		},
		Actee: policy.Datasource{
			Actor: "Digitale Huis X",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "calamity",
				Authority: policy.Datasource{
					Actor: "some-authority",
					Url:   "http://localhost:8088/some",
				},
				Description: "Because Simon says so",
			},
		},
		Goal: "To test this",
	}
	reqBody, err := json.Marshal(testPolicyRequest)
	assert.NoError(t, err)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockRepository(ctrl)

	ctx := context.Background()
	var e error

	var repoResp = policy.Policy{
		PolicyID: 42,
		Actor:    "Dummy user",
		Action: policy.Action{
			Scope:       "read:someinformation",
			Description: "Read some information",
		},
		Actee: policy.Datasource{
			Actor: "Digitale Huis X",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "calamity",
				Authority: policy.Datasource{
					Actor: "some-bla",
					Url:   "http://localhost:8088/some",
				},
				Description: "Because Simon says so",
			},
		},
		Goal: "To test this",
	}
	repo.EXPECT().Create(ctx, testPolicyRequest).Return(&repoResp, e).AnyTimes()

	router := chi.NewRouter()

	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/api/v1/policies", bytes.NewBuffer(reqBody))

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotPolicy policy.PolicyResponse
	err = json.Unmarshal(body, &gotPolicy)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusCreated, res.Code)
	assert.Equal(t, repoResp.PolicyID, gotPolicy.PolicyID)
	assert.Equal(t, repoResp.Actor, gotPolicy.Actor)
	assert.Equal(t, repoResp.Action, gotPolicy.Action)
	assert.Equal(t, repoResp.Actee, gotPolicy.Actee)
	assert.Equal(t, repoResp.Conditions, gotPolicy.Conditions)
	assert.Equal(t, repoResp.Goal, gotPolicy.Goal)
}

func TestHandler_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()

	repo := mock.NewMockRepository(ctrl)
	var id int64 = 42
	var repoResp = policy.Policy{
		PolicyID: id,
		Actor:    "Dummy user",
		Action: policy.Action{
			Scope:       "read:someinformation",
			Description: "Read some information",
		},
		Actee: policy.Datasource{
			Actor: "Digitale Huis X",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "calamity",
				Authority: policy.Datasource{
					Actor: "some-bla",
					Url:   "http://localhost:8088/some",
				},
				Description: "Because Simon says so",
			},
		},
		Goal: "To test this",
	}
	repo.EXPECT().Read(ctx, id).Return(&repoResp, nil).AnyTimes()

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/v1/policies/%d", id), nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotPolicy policy.PolicyResponse
	err = json.Unmarshal(body, &gotPolicy)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, repoResp.PolicyID, gotPolicy.PolicyID)
	assert.Equal(t, repoResp.Actor, gotPolicy.Actor)
	assert.Equal(t, repoResp.Action, gotPolicy.Action)
	assert.Equal(t, repoResp.Actee, gotPolicy.Actee)
	assert.Equal(t, repoResp.Conditions, gotPolicy.Conditions)
	assert.Equal(t, repoResp.Goal, gotPolicy.Goal)
}

func TestHandler_All(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockRepository(ctrl)
	var id int64 = 42
	var repoResp = policy.Policy{
		PolicyID: id,
		Actor:    "Dummy user",
		Action: policy.Action{
			Scope:       "read:someinformation",
			Description: "Read some information",
		},
		Actee: policy.Datasource{
			Actor: "Digitale Huis X",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "calamity",
				Authority: policy.Datasource{
					Actor: "some-bla",
					Url:   "http://localhost:8088/some",
				},
				Description: "Because Simon says so",
			},
		},
		Goal: "To test this",
	}

	ctx := context.Background()
	f := &policy.Filter{
		Base: filter.Filter{
			Page:          0,
			Size:          10,
			DisablePaging: false,
			Search:        false,
		},
	}
	var policies []*policy.Policy = []*policy.Policy{&repoResp}

	repo.EXPECT().All(ctx, f).Return(policies, nil).AnyTimes()

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/api/v1/policies?page=1&size=10", nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotPolicy []policy.PolicyResponse
	err = json.Unmarshal(body, &gotPolicy)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, repoResp.PolicyID, gotPolicy[0].PolicyID)
	assert.Equal(t, repoResp.Actor, gotPolicy[0].Actor)
	assert.Equal(t, repoResp.Action, gotPolicy[0].Action)
	assert.Equal(t, repoResp.Actee, gotPolicy[0].Actee)
	assert.Equal(t, repoResp.Conditions, gotPolicy[0].Conditions)
	assert.Equal(t, repoResp.Goal, gotPolicy[0].Goal)
}

func TestHandler_Update(t *testing.T) {
	var id int64 = 42
	testPolicyRequest := &policy.Policy{
		PolicyID: id,
		Actor:    "Dummy user",
		Action: policy.Action{
			Scope:       "read:someinformation",
			Description: "Read some information",
		},
		Actee: policy.Datasource{
			Actor: "Digitale Huis X",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "calamity",
				Authority: policy.Datasource{
					Actor: "some-authority",
					Url:   "http://localhost:8088/some",
				},
				Description: "Because Simon says so",
			},
		},
		Goal: "To test this",
	}
	reqBody, err := json.Marshal(testPolicyRequest)
	assert.NoError(t, err)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockRepository(ctrl)

	ctx := context.Background()
	var repoResp = policy.Policy{
		PolicyID: id,
		Actor:    "Dummy user",
		Action: policy.Action{
			Scope:       "read:someinformation",
			Description: "Read some information",
		},
		Actee: policy.Datasource{
			Actor: "Digitale Huis X",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "calamity",
				Authority: policy.Datasource{
					Actor: "some-authority",
					Url:   "http://localhost:8088/some",
				},
				Description: "Because Simon says so",
			},
		},
		Goal: "To test this",
	}
	repo.EXPECT().Update(ctx, testPolicyRequest).Return(nil).AnyTimes()

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodPut, fmt.Sprintf("/api/v1/policies/%d", id), bytes.NewBuffer(reqBody))
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotPolicy policy.PolicyResponse
	err = json.Unmarshal(body, &gotPolicy)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, repoResp.PolicyID, gotPolicy.PolicyID)
	assert.Equal(t, repoResp.Actor, gotPolicy.Actor)
	assert.Equal(t, repoResp.Action, gotPolicy.Action)
	assert.Equal(t, repoResp.Actee, gotPolicy.Actee)
	assert.Equal(t, repoResp.Conditions, gotPolicy.Conditions)
	assert.Equal(t, repoResp.Goal, gotPolicy.Goal)
}

func TestHandler_Delete(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockRepository(ctrl)

	ctx := context.Background()
	var id int64 = 42

	repo.EXPECT().Delete(ctx, id).Return(nil).AnyTimes()

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, repo)

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("/api/v1/policies/%d", id), nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)

	assert.Equal(t, http.StatusOK, res.Code)
}
